using Microsoft.EntityFrameworkCore;
using MasterDataProducao.Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataProducao.Shared.Context
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):base(options)
        {}


        public DbSet<Produto> Produtos {get; set;}
        public DbSet<PlanoFabrico> PlanosFabrico {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.ApplyConfiguration(new ProdutoEntityTypeConfiguration());
        }

        private class ProdutoEntityTypeConfiguration : IEntityTypeConfiguration<Produto>
        {
            public void Configure(EntityTypeBuilder<Produto> produtoConfiguration)
            {
                produtoConfiguration
                    .HasOne(a => a.PlanoFabrico)
                    .WithOne(b => b.Produto)
                    .HasForeignKey<PlanoFabrico>( b => b.ProdutoId);
            }
        }


    }
    
}