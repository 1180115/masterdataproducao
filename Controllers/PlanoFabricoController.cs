﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MasterDataProducao.Domain.Interfaces;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Domain.Commands;

namespace MasterDataProducao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanoFabricoController : ControllerBase
    {
        private readonly IPlanoFabricoService _planofabricoService;
        private readonly IPlanoFabricoRepository _planofabricoRepository;
        public PlanoFabricoController(
            IPlanoFabricoService planoFabricoService,
            IPlanoFabricoRepository planoFabricoRepository)
        {
            _planofabricoService = planoFabricoService;
            _planofabricoRepository = planoFabricoRepository;
        }
        // GET api/planofabrico
        [HttpGet]
        public async Task<ActionResult<List<PlanoFabrico>>> GetAll()
        {
            return await _planofabricoRepository.getPlanosFabrico();
        }

        // GET api/planofabrico/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlanoFabrico>> Get([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Invalid Id");
            }
            return await _planofabricoRepository.getPlanoFabricoById(id);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddPlanoFabricoCommand command)
        {
            PlanoFabrico planoFabrico = await _planofabricoService.AddPlanoFabrico(command);
            
            if (planoFabrico == null)
            {
                return BadRequest("Invalid Plano Fabrico");
            }
            return Created("planoFabrico /"+ planoFabrico.Id, planoFabrico);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
