using System.Threading.Tasks;
using MasterDataProducao.Domain.Commands;
using MasterDataProducao.Domain.Interfaces;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Infra.Interfaces;



namespace MasterDataProducao.Domain.Services
{
    public class PlanoFabricoService:IPlanoFabricoService
    {
        private readonly IPlanoFabricoRepository _planofabricoRepository;
        public PlanoFabricoService(IPlanoFabricoRepository planofabricoRepository)
        {
            _planofabricoRepository = planofabricoRepository;
        }
        public async Task<PlanoFabrico> AddPlanoFabrico(AddPlanoFabricoCommand command)
        {
            PlanoFabrico planoFabrico = new PlanoFabrico(new Produto(command.produto), command.ProdutoId );
            PlanoFabrico planoFabricoResult = await _planofabricoRepository.addPlanoFabrico(planoFabrico);

            return planoFabricoResult;
        }
    }
}

    
