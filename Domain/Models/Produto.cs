using System.Collections.Generic;
using MasterDataProducao.Shared.Interfaces;

namespace MasterDataProducao.Domain.Models
{
    public class Produto : Entity
    {
        public PlanoFabrico PlanoFabrico {get; private set;}

        public Produto(PlanoFabrico planofabrico)
        {
            PlanoFabrico = planofabrico;
        }
        protected Produto ()
        {}
    }
}