using System;
using System.Collections.Generic;
using MasterDataProducao.Shared.Interfaces;

namespace MasterDataProducao.Domain.Models
{
    public class PlanoFabrico:Entity
    {
        public Produto Produto {get; private set;}
        public Guid ProdutoId  {get; private set;}
        
        public PlanoFabrico (Produto produto, Guid produtoId)
        {
            Produto = produto;
            ProdutoId = produtoId;
        }
        protected PlanoFabrico()
        {}
    }
}