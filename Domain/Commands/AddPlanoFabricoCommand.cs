using System;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Shared.Command;

namespace MasterDataProducao.Domain.Commands
{
    public class AddPlanoFabricoCommand:ICommand
    {
        public Produto produto;
        public Guid ProdutoId;

        public bool Validate()
        {
            if(ProdutoId == Guid.Empty || produto == null)
            {
                return false;
            }
            return true;
        }

        
    }
    
}