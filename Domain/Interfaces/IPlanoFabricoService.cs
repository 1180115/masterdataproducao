using System.Threading.Tasks;
using MasterDataProducao.Domain.Commands;
using MasterDataProducao.Domain.Models;

namespace MasterDataProducao.Domain.Interfaces
{
    public interface IPlanoFabricoService
    {
        Task<PlanoFabrico> AddPlanoFabrico(AddPlanoFabricoCommand command);
    }
}